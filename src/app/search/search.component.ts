import { Component, OnInit } from '@angular/core';
import { DataHandlerServiceService } from '../data-handler-service.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  userName:string;
  allUsers:any;
  users:any;

  constructor(private dataHandlerService: DataHandlerServiceService) {
    this.dataHandlerService.getusers().subscribe((response)=>
      {
        this.allUsers = response;
      });
   }

  ngOnInit() { }

  filterUsers():void{
    this.users = this.allUsers.filter( user => 
      user.name.toLowerCase().indexOf(this.userName.toLowerCase())> -1
    );
  }
}
