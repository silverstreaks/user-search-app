import { TestBed } from '@angular/core/testing';

import { DataHandlerServiceService } from './data-handler-service.service';

describe('DataHandlerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataHandlerServiceService = TestBed.get(DataHandlerServiceService);
    expect(service).toBeTruthy();
  });
});
